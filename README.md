# 利用方法

node v9.11.2 で実装しました。

`npm install`ののち、`node main.js`で起動します。


# データ・フォーマット

下記のフォーマットにて進めさせていただきます。

```json
{
  "light": { // 全天日照量
    "value": 0.37653676668611813, // float, 0-1の範囲
    "timestamp": "Fri Sep 20 2019 15:44:50 GMT+0900 (GMT+09:00)"
  },
  "water": { // 降水量
    "value": 0.6736204394573164, // float, 0-1の範囲
    "timestamp": "Fri Sep 20 2019 15:44:50 GMT+0900 (GMT+09:00)"
  },
  "co2": { // 二酸化炭素排出量
    "value": 0.4199020998359273, // float, 0-1の範囲
    "timestamp": "Fri Sep 20 2019 15:44:50 GMT+0900 (GMT+09:00)"
  },
  "human": { // 人口密度
    "value": 0.0065231612319185395, // float, 0-1の範囲
    "timestamp": "Fri Sep 20 2019 15:44:50 GMT+0900 (GMT+09:00)"
  }
}
```

# 値の推移

先週一週間の値の推移を下記にまとめてあります。

https://paper.dropbox.com/doc/191003--Al5mI0EHeazCwxMvuFY7oOWrAg-a6yLhlPWClA0Sa1JLOuMh