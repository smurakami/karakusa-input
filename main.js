const schedule = require('node-schedule');
const moment = require('moment');
const fs = require('fs')


const light = require('./components/light');
const rain = require('./components/rain');
const co2 = require('./components/co2');
const human = require('./components/human');


function saveData(data, log=true) {
  const debug = false;
  data = JSON.parse(JSON.stringify(data));

  if (!debug) {
    for (key in data) {
      delete data[key].debug;
    }
  }

  // 10分ごとのファイル名に限定
  let now = moment()
  let min = now.minutes();
  let delta = min % 10;
  now.subtract(delta, 'minutes');

  fs.writeFileSync(
    './json/current.json',
    JSON.stringify(data, null, '  '))

  if (log) {
    fs.writeFileSync(
      './json/log/' + now.format("YYYY-MM-DD-HH-mm") + '.json',
      JSON.stringify(data, null, '  '))
  }
}


async function main() {
  const current = await Promise.all([
    light.getData(),
    rain.getData(),
    co2.getData(),
    human.getData(),
  ]).then(list => ({
    light: list[0],
    rain: list[1],
    co2: list[2],
    human: list[3],
  }));

  saveData(current, false);
  console.log('*****************');
  console.log('initialized current data:');
  console.log(moment().format());
  console.log(current);
  console.log();

  /*
    光量:
      - 1時間に一度、毎時45分にデータを取得します。
          - ＊使用するデータは、毎時40分頃にデータが更新される仕様です
    降水量:
      - 10分に一度、データを取得します。
          - ＊使用するデータは、10分ごとにデータが更新される仕様です
    交通量:
      - データの取得頻度
          - 30分に一度、データを取得します。
    人の多さ: 
      - 20分に一度、データを取得します。
          - ＊使用するデータは、20分ごとにデータが更新される仕様です

   */

  schedule.scheduleJob('50 * * * *', async function(){
    const data = await light.getData();

    console.log('*****************');
    console.log('update light:');
    console.log(moment().format());
    console.log(data)
    console.log();

    current.light = data;
    saveData(current)
  });

  schedule.scheduleJob('*/10 * * * *', async function(){
    const data = await rain.getData();

    console.log('*****************');
    console.log('update rain:');
    console.log(moment().format());
    console.log(data)
    console.log();

    current.rain = data;
    saveData(current)
  });

  schedule.scheduleJob('*/30 * * * *', async function(){
    const data = await co2.getData();

    console.log('*****************');
    console.log('update co2:');
    console.log(moment().format());
    console.log(data)
    console.log();

    current.co2 = data;
    saveData(current)
  });

  schedule.scheduleJob('*/20 * * * *', async function(){
    const data = await human.getData();

    console.log('*****************');
    console.log('update human:');
    console.log(moment().format());
    console.log(data)
    console.log();

    current.human = data;
    saveData(current)
  });
}


main()
