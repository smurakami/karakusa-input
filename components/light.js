const ut = require('./util.js')
const rp = require('request-promise');
const { JSDOM } = require('jsdom');
const moment = require('moment-timezone');
moment.tz.setDefault("Asia/Tokyo"); // タイムゾーンを念押し

/**
 * 現在時刻の全天日射量を取得する
 */

async function getSunLightData() {
  const url = 'https://www.data.jma.go.jp/obd/stats/data/mdrr/synopday/data1s.html';
  const body = await rp.get(url);

  const dom = new JSDOM(body, {
    url: url,
  });

  const document = dom.window.document;
  const tables = document.querySelectorAll('table');

  let target_row = null;
  for (let table of tables) {
      for (let row of table.rows) {
          const prefecture = row.cells[0].textContent;
          if (prefecture == "東京") {
              target_row = row;
              break
          }
      }
  }

  const sun_index = 22;
  const raw_result = target_row.cells[sun_index].textContent;  
  const raw_result_num = raw_result.match(/[+-]?(?:\d+\.?\d*|\.\d+)/); // extract number
  const value = Number(raw_result_num);

  const time_info = document.getElementById('main').querySelector('div').textContent;

  return {value, raw_result, time_info};
}

/**
 * 時刻をfloat (or string)で受け取り、hour, minutesにして返す
 */

function floatToHourMinute(value) {
  value = Number(value);

  let hour = Math.floor(value);
  let minute = Math.floor((value - hour) * 60);
  return { hour, minute };
}

/**
 * 日の出、日の入りの時刻を取得してmomentオブジェクトにして返す。
 */
async function getSunRiseSet(now) {
  let ginza_shiseido = {
    lat: 35.66986,
    lng: 139.76134,
  };

  const res = await rp.get('http://labs.bitmeister.jp/ohakon/api/?', {
    qs: {
      mode: "sun_rise_set",
      year: now.year(),
      month: now.month() + 1,
      day: now.date(),
      ...ginza_shiseido,
      },
    }
  );

  const data = new JSDOM(res);
  const sunrise = floatToHourMinute(data.window.document.querySelector('sunrise').textContent);
  const sunset = floatToHourMinute(data.window.document.querySelector('sunset').textContent);

  return {
    sunrise: now.clone().set({...sunrise, second: 0, millisecond: 0}),
    sunset: now.clone().set({...sunset, second: 0, millisecond: 0}),
  }
}

/**
 * 日照のデータを取得する関数
 */


let prev_info = null;

async function getData() {
  const now = moment();
  const sunlight = await getSunLightData();
  const {sunrise, sunset} = await getSunRiseSet(now);
  const raw_value = sunlight.value;

  let info = {
    time_info: sunlight.time_info,
    moment: now,
    value: raw_value,    
    diff: null,
  }

  const diff = get_diff(info, prev_info);
  info.diff = diff;
  prev_info = info;

  let {value, is_active} = fix_sunrise_sunset(diff, sunrise, sunset, now)
  value = normalize(value);

  let debug = { // デバッグ用
    sunlight,
    sunrise,
    sunset,
    raw_value,
    is_active,
  }

  return {
    value, 
    timestamp: now.format(),
    debug,
  }
}

exports.getData = getData
exports.fix_sunrise_sunset = fix_sunrise_sunset
exports.normalize = normalize
exports.get_diff = get_diff

function get_diff(info, prev_info) {
  // 全天日射量だけは、微分値を使わないと行けないので、ここで処理する。
  // 前の値が無い時(起動時)は、日の出から一定の日射と仮定して計算する。
  // 例外処理なので、日の出の時刻は6時に固定してしまう。
  // (日の出の時刻をウェブ経由で取得すると、煩雑化するため)
  // info構造
  // {
  //   time_info: 気象庁ウェブ掲載の時間情報,
  //   moment: momentによるタイムスタンプ
  //   value: 生の値
  //   diff: prevのみ、微分値
  // }

  if (prev_info != null) {
    if (info.time_info == prev_info.time_info) {
      return prev_info.diff
    } else {
      let hours = info.moment.diff(prev_info.moment, 'hours')
      if (hours < 1) { // 日の出前
        hours = 1
      }

      let diff = (info.value -  prev_info.value) / hours

      if (diff < 0) { // 日照がマイナスになることはない。原理上無いが、日をまたいだりしたらおきる。
        diff = 0;
      }
      return diff;
    }
  } else {
    // 起動時など
    let hours = info.moment.hours() - 6;
    if (hours < 1) { // 日の出前
      hours = 1
    }
    return info.value / hours;
  }
}

function fix_sunrise_sunset(value, sunrise, sunset, now) {
  // 日の出前、日の出後は手動でゼロに
  // 太陽が出ている時は、全天日射量をウェブから取得し、
  // 太陽が沈んでいる時は、0にする。
  // ただし、太陽が沈む前後1.5時間は、全天日照量の値を使う。
  const is_after_sunrise = sunrise.clone().subtract({minutes: 90}) < now;
  const is_before_sunset = sunset.clone().add({minutes: 90}) > now;

  let is_active;
  if ( is_after_sunrise && is_before_sunset ) {
    value = value;
    is_active = true;
  } else {
    value = 0.0; // 一定の値
    is_active = false;
  }

  return {value, is_active }
}

function normalize(value) {
  value = ut.proj(value, 0, 4);
  value = ut.proj(value, 0, 0.7);
  return ut.clip(value);
}

if (require.main === module) {
  getData().then(console.log).catch(console.error);
}

