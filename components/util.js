exports.clip = function(value, min=0, max=1) {
  if (value < min) {
    return min;
  } else if (value > max) {
    return max;
  } else {
    return value;
  }
}


exports.proj = function(value, from_min, from_max, to_min=0, to_max=1) {
  value = (value - from_min) / (from_max - from_min)  // 0-1に
  value = value * (to_max - to_min) + to_min
  return value;
}

exports.sum = function(array) {
  let sum = 0;
  array.forEach(e => sum += e)
  return sum;
}

exports.mean = function(array) {
  return exports.sum(array) / array.length;
}
