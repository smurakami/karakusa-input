const rp = require('request-promise');
const ut = require('./util')
const { JSDOM } = require('jsdom');
const iconv = require('iconv-lite')

const moment = require('moment-timezone');
moment.tz.setDefault("Asia/Tokyo"); // タイムゾーンを念押し

/**
 * 現在時刻の全天日射量を取得する
 */
async function getRainData() {
  const url = "https://www.data.jma.go.jp/obd/stats/data/mdrr/pre_rct/alltable/pre1h00_rct.csv";
  const buffer = await rp.get(url, {encoding: null});
  const text = iconv.decode(buffer, "Shift_JIS");

  const data = text.split('\r\n').map(col => col.split(','))

  const head = data[0];
  const body = data.slice(1);
  const index = body.findIndex(col => (col[1] == "東京都") && (col[2] == "東京（トウキョウ）"))
  const row = body[index];

  // 確認用
  // for (var i = 0; i < head.length; i++) {
  //    console.log(`${i} ${head[i]}: ${row[i]}`)
  // }

  const target_index = 9;
  const value = Number(row[target_index]);

  return {
    value,
    // デバッグ用に保存しておく。
    full_data: {
      head,
      row,      
    }
  };
}

/**
 * 降水量のデータを取得
 */

async function getData() {
  const rain = await getRainData();
  const raw_value = rain.value;
  const value = normalize(rain.value);

  return {
    value,
    timestamp: moment().format(),
    debug: {
      full_data: rain.full_data,
      raw_value,
    },
  }
}

function normalize(value) {
  if (value < 3) {
    // 弱い雨
    value = ut.proj(value, 0, 3, 0, 0.4)
  } else if (value < 10) {
    // 普通の雨
    value = ut.proj(value, 3, 10, 0.4, 0.8)
  } else {
    // 強い雨
    value = ut.proj(value, 10, 14, 0.8, 1.0)
  }

  return ut.clip(value)
}

exports.getData = getData
exports.normalize = normalize

if (require.main === module) {
  getData().then(console.log).catch(console.error);
}
