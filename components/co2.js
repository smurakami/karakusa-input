const rp = require('request-promise');
const ut = require('./util')
const { JSDOM } = require('jsdom');
const iconv = require('iconv-lite')
const fs = require('fs');
const holiday_jp = require('@holiday-jp/holiday_jp');

const moment = require('moment-timezone');
moment.tz.setDefault("Asia/Tokyo"); // タイムゾーンを念押し

const api_key = "AIzaSyCaKHenHwZrOpKdeOgkta6RJLkB2TIQ0DM"; // Internのものを流用
const googleMaps = require('@google/maps').createClient({
  key: api_key,
  Promise: Promise
});


async function getTrafficData() {
  const ginza_icchome = [35.673839, 139.767774];
  const ginza_nanachome = [35.668532, 139.762198];
  const now = moment();

  const res = await googleMaps.directions({
    origin: ginza_icchome,
    destination: ginza_nanachome,
    departure_time: now.unix(),
    mode: 'driving',
    traffic_model: 'best_guess'
  }).asPromise()

  const data = res.json;
  const distance = data.routes[0].legs[0].distance.value; // m
  const duration = data.routes[0].legs[0].duration_in_traffic.value; // sec
  // const speed = distance / duration; // m / sec

  const value = duration / distance; // sec/m

  return value;
}


function applyHokoten(value, now) {
  const is_holiday = holiday_jp.isHoliday(now.toDate()) || ([0, 6].includes(now.weekday()));
  const is_in_time = (12 < now.hours()) && (now.hours() < 17);

  const is_hokoten = is_holiday && is_in_time;

  if (is_hokoten) {
    return 0;
  } else {
    return value;
  }
}

function normalize(value) {
  value = ut.proj(value, 0.1, 0.6);
  value = ut.proj(value, 0, 0.9);
  return ut.clip(value);
}

/**
 * CO2データを種痘
 */

async function getData() {
  // const rain = await getRainData();
  var now = moment();
  // var inOneHour = Math.round(now/1000);  

  let value = await getTrafficData();
  const raw_value = value;

  value = applyHokoten(value, now);
  value = normalize(value);

  return {
    value: value, 
    timestamp: moment().format(),
    debug: {
      raw_value,
    },
  }
}

exports.getData = getData
exports.normalize = normalize
exports.applyHokoten = applyHokoten

if (require.main === module) {
  getData().then(console.log).catch(console.error);
}
