const rp = require('request-promise');
const ut = require('./util.js')
const Jimp = require('jimp')
const tinycolor = require("tinycolor2");
const moment = require('moment-timezone');
moment.tz.setDefault("Asia/Tokyo"); // タイムゾーンを念押し

async function get_mask() {
  const mask = await Jimp.read(__dirname + '/human_mask.png');

  let values = [];

  for (let i = 0; i < mask.getHeight(); i++) {
    for (let j = 0; j < mask.getWidth(); j++) {
      const color = Jimp.intToRGBA(mask.getPixelColor(j, i))
      const value = (color.r / 255 + color.g / 255 + color.b / 255) / 3;
      values.push(value);
    }
  }

  const sum = ut.sum(values);
  values = values.map((e) => e / sum) // 正規化しておく
  return values;
}

async function getHumanData() {
  const now = moment();

  now.subtract({minutes: 20}); // 新しすぎるデータは、404になるようなので、20分バッファを持っておく。
  const minutes = now.minutes();

  const date_str = now.format('YYYYMMDDHH') + String(Math.floor(minutes / 20) * 20).padStart(2, '0');

  const url = 'https://map.c.yimg.jp/jam?x=29105&y=3479&z=16&date=' + date_str + '&size=406';
  const data = await rp.get(url, {encoding: null});
  const image = await Jimp.read(data);

  // 解像度が余分にあるので、適度に縮小
  image.resize(64, 64);

  const hues = [];

  for (let i = 0; i < image.getHeight(); i++) {
    for (let j = 0; j < image.getWidth(); j++) {
      var color = Jimp.intToRGBA(image.getPixelColor(j, i))
      color = tinycolor.fromRatio({
        r: color.r / 255,
        g: color.g / 255,
        b: color.b / 255,
        a: color.a / 255,
      })
      color = color.toHsv()
      hues.push(color.h)
    }
  }

  const mean = hues.reduce((a, b) => a + b, 0) / hues.length
  const min = Math.min(...hues);
  const max = Math.max(...hues);

  return {
    value: mean,
    min,
    max,
    url,
    hues,
  }
}

async function applyMask(hues) {
  // hue情報を処理した上で、銀座通りに限定する。
  const mask = await get_mask();
  return ut.sum(hues.map((hue) => {
    hue = hue - 300;
    if (hue < 0) {
      hue += 360;
    }
    hue = 360 - hue
    hue = hue / 360
    return hue
  }).map((value, i) => value * mask[i]))
}

function normalize(value) {
  x = ut.proj(value, 0.1, 0.6)
  x = x * 2 - 1
  x = Math.asin(x)
  x = Math.pow(x, 2) * Math.sign(x)
  x = x / (Math.PI / 2)
  x = (x + 1) / 2

  x = ut.proj(x, 0.3, 0.9); // 191007追加。
  return ut.clip(x)
}


/**
 *  人口密度のデータを取得
 */

async function getData() {
  const human = await getHumanData();
  const raw_value = human.value;

  let value = await applyMask(human.hues);
  value = normalize(value)

  return {
    value,
    timestamp: moment().format(),
    debug: {
      raw_value,
      min: human.min,
      max: human.max,
      url: human.url,
      hues: human.hues,
    },
  }
}

exports.getData = getData
exports.normalize = normalize
exports.applyMask = applyMask;

if (require.main === module) {
  getData().then(console.log).catch(console.error);
}

